﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TweetSharp;
using System.Diagnostics;
using InputKey;
using MySql.Data.MySqlClient;
using Hammock;
using System.IO;
using System.Threading;

namespace TwitterBot
{
    public partial class Bot : Form
    {
        public static String _consumerKey = "";
        public static String _consumerSecret = "";
        public String txtResult;
        TwitterService service;
        private DBConnect dbConnect = new DBConnect();
        private List<string> cache_hashtags;
        private List<string> cache_object;
        private List<string> cache_predicate;
        private List<string> cache_subject;
        private Int64 lastTime = 0;

        public Bot()
        {
            InitializeComponent();
        }

        private void btn_start_stop_Click(object sender, EventArgs e)
        {
            post_now();
            post_timer.Start();
            btn_start_stop.Text = "Stop";
        }

        private void register()
        {
            // In v1.1, all API calls require authentication
            service = new TwitterService(_consumerKey, _consumerSecret);

            // Step 1 - Retrieve an OAuth Request Token
            OAuthRequestToken requestToken = service.GetRequestToken();

            // Step 2 - Redirect to the OAuth Authorization URL
            Uri uri = service.GetAuthorizationUri(requestToken);
            Process.Start(uri.ToString());

            // Step 3 - Exchange the Request Token for an Access Token
            string verifier = InputDialog.mostrar("Bitte gebe hier den code ein:", "Authoresieren", InputDialog.ACEPTAR_BOTON); // <-- This is input into your application by your user
            OAuthAccessToken access = service.GetAccessToken(requestToken, verifier);

            // Step 4 - User authenticates using the Access Token
            service.AuthenticateWith(access.Token, access.TokenSecret);
        }

        private void post_now()
        {
            Int64 timestamp = Convert.ToInt64(UnixTimestampFromDateTime(DateTime.Now));
            var date = DateTime.Now;
            Int64 current_minutes = Convert.ToInt64(date.Minute);
            this.Text = Convert.ToString("Twitter Bot (" + current_minutes + ")");
            if (lastTime + 120 < timestamp) //120 Sekunden Pause
            {
                if (current_minutes == 15 || current_minutes == 30 || current_minutes == 45 || current_minutes == 0) //
                {
                    String string_full = "";
                    Random rnd = new Random();
                    int num_hashtags = rnd.Next(cache_hashtags.Count);
                    int num_object = rnd.Next(cache_object.Count);
                    int num_predicate = rnd.Next(cache_predicate.Count);
                    int num_subject = rnd.Next(cache_subject.Count);

                    String string_hashtags = cache_hashtags[num_hashtags];
                    String string_object = cache_object[num_object];
                    String string_predicate = cache_predicate[num_predicate];
                    String string_subject = cache_subject[num_subject];

                    string_full = string_subject + " " + string_predicate;

                    if (rnd.Next(0, 11) <= 9)
                    {
                        string_full += " " + string_object;
                    }

                    if (rnd.Next(0, 11) <= 5)
                    {
                        string_full += " " + string_hashtags;
                    }

                    Console.WriteLine(string_full);
                    last.Text = string_full;
                    if (string_full != "")
                    {
                        if (rnd.Next(0, 11) <= 6)
                        {
                            Console.WriteLine("RandomImage");
                            SendTweetWithMediaOptions STWMoptions = new SendTweetWithMediaOptions();
                            Bitmap img = randomImage();
                            Thread.Sleep(500);
                            MemoryStream ms = new MemoryStream();
                            img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            ms.Seek(0, SeekOrigin.Begin);
                            Dictionary<string, Stream> images = new Dictionary<string, Stream> { { "RandomImage", ms } };
                            STWMoptions.Status = string_full;
                            STWMoptions.Images = images;
                            service.SendTweetWithMedia(STWMoptions);
                            
                        }
                        else
                        {
                            Console.WriteLine("NORMAL");
                            SendTweetOptions options = new SendTweetOptions();
                            options.Status = string_full;
                            service.SendTweet(options);
                        }

                        lastTime = timestamp;
                    }
                }
            }
        }

        private Bitmap randomImage()
        {
            int width = 640, height = 320;

            //bitmap
            Bitmap bmp = new Bitmap(width, height);

            //random number
            Random rand = new Random();

            //create random pixels
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    //generate random ARGB value
                    int a = rand.Next(256);
                    int r = rand.Next(256);
                    int g = rand.Next(256);
                    int b = rand.Next(256);

                    //set ARGB value
                    bmp.SetPixel(x, y, Color.FromArgb(a, r, g, b));
                }
            }

            //load bmp in picturebox1
            pictureBox1.Image = bmp;

            //save (write) random pixel image
            bmp.Save("RandomImage.jpg");
            return bmp;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cachetimer.Start();
            updateCache();
            register();
        }

        private void updateCache()
        {
            cache_hashtags = dbConnect.Select("hashtags");
            cache_object = dbConnect.Select("object");
            cache_predicate = dbConnect.Select("predicate");
            cache_subject = dbConnect.Select("subject");
            Console.WriteLine("Cache refresh");
        }

        private void cachetimer_Tick(object sender, EventArgs e)
        {
            updateCache();
        }

        private void post_timer_Tick(object sender, EventArgs e)
        {
            post_now();
        }

        public String UnixTimestampFromDateTime(DateTime date)
        {
            long unixTimestamp = date.Ticks - new DateTime(1970, 1, 1).Ticks;
            unixTimestamp /= TimeSpan.TicksPerSecond;
            return unixTimestamp.ToString();
        }
    }
}
