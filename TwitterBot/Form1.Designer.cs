﻿namespace TwitterBot
{
    partial class Bot
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.last = new System.Windows.Forms.RichTextBox();
            this.post_timer = new System.Windows.Forms.Timer(this.components);
            this.btn_start_stop = new System.Windows.Forms.Button();
            this.cachetimer = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // last
            // 
            this.last.Location = new System.Drawing.Point(12, 12);
            this.last.Name = "last";
            this.last.ReadOnly = true;
            this.last.Size = new System.Drawing.Size(178, 21);
            this.last.TabIndex = 0;
            this.last.Text = "";
            // 
            // post_timer
            // 
            this.post_timer.Interval = 2000;
            this.post_timer.Tick += new System.EventHandler(this.post_timer_Tick);
            // 
            // btn_start_stop
            // 
            this.btn_start_stop.Location = new System.Drawing.Point(196, 10);
            this.btn_start_stop.Name = "btn_start_stop";
            this.btn_start_stop.Size = new System.Drawing.Size(48, 23);
            this.btn_start_stop.TabIndex = 1;
            this.btn_start_stop.Text = "Start";
            this.btn_start_stop.UseVisualStyleBackColor = true;
            this.btn_start_stop.Click += new System.EventHandler(this.btn_start_stop_Click);
            // 
            // cachetimer
            // 
            this.cachetimer.Interval = 1800000;
            this.cachetimer.Tick += new System.EventHandler(this.cachetimer_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 39);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(229, 58);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // Bot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(253, 109);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn_start_stop);
            this.Controls.Add(this.last);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Bot";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox last;
        private System.Windows.Forms.Timer post_timer;
        private System.Windows.Forms.Button btn_start_stop;
        private System.Windows.Forms.Timer cachetimer;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

